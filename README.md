# welcomebot

Un repo pour noter la configuration du plugin Mattermost welcomebot

Modify your `config.json` file to include your Welcome Bot's messages and actions, under the `PluginSettings`.

Voir : https://github.com/mattermost/mattermost-plugin-welcomebot/
